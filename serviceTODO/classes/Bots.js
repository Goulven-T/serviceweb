const Bot = require("./Bot.js");
const data = require("./data.json");


class Bots{
  constructor(){
    this.Bots = new Map();
    data.forEach((item, index, array) => {
      let newBot = new Bot(item);
      this.Bots.set(newBot.id,newBot);
    });
  }
  get size(){
    return this.Bots.size;
  }
  addBot(bot){
    let newBot = new Bot(bot);
    console.log("addBot :"+JSON.stringify(newBot));
    this.Bots.set(newBot.id,newBot);
    data.add(newBot);
    return this.getBot(newBot.id);
  }
  getBot(id){
    this.Bots.forEach(logMapElements);
    console.log(typeof id);
    console.log("getting Bot with id "+id+" : "+JSON.stringify(this.Bots.get(id)));
    return this.Bots.get(id);
  }
  deleteBot(id){
    this.Bots.forEach(logMapElements);
    let Bot = this.Bots.get(id);
	console.log("Bot :"+JSON.stringify(Bot));
    if(undefined!=Bot){
      this.Bots.delete(id);
      return id;
    } else {
      return undefined;
    }
  }
  updateBot(updatedBot){
    const hasBot = this.Bots.has(updatedBot.id);
    if(hasBot){
      this.Bots.set(updatedBot.id,updatedBot);
      return updatedBot;
    } else {
      return undefined;
    }
  }
  getBots(){
    let tabBots = [];
    for (const v of this.Bots.values()) {
      tabBots.push(v);
    }
    return tabBots;
  }
  deleteBots(){
    this.Bots.clear();
  }

}

function logMapElements(value, key, map) {
  console.log("m["+key+"] = "+JSON.stringify(value));
}


module.exports = Bots;
