const Tache = require("./Tache.js");
const data = require("./data.json");


class Bots{
  constructor(){
    this.Bot = new Map();
    data.forEach((item, index, array) => {
      let newBot = new Bot(item);
      this.Bot.set(newBot.id,newBot);
    });
  }
  get size(){
    return this.Taches.size;
  }
  addTache(tache){
    let newTache = new Tache(tache);
    console.log("addTache :"+JSON.stringify(newTache));
    this.Taches.set(newTache.id,newTache);
    return this.getTache(newTache.id);
  }
  getTache(id){
    this.Taches.forEach(logMapElements);
    console.log(typeof id);
    console.log("getting Tache with id "+id+" : "+JSON.stringify(this.Taches.get(id)));
    return this.Taches.get(id);
  }
  deleteTache(id){
    this.Taches.forEach(logMapElements);
    let Tache = this.Taches.get(id);
	console.log("Tache :"+JSON.stringify(Tache));
    if(undefined!=Tache){
      this.Taches.delete(id);
      return id;
    } else {
      return undefined;
    }
  }
  updateTache(updatedTache){
    const hasTache = this.Taches.has(updatedTache.id);
    if(hasTache){
      this.Taches.set(updatedTache.id,updatedTache);
      return updatedTache;
    } else {
      return undefined;
    }
  }
  getTaches(){
    let tabTaches = [];
    for (const v of this.Taches.values()) {
      tabTaches.push(v);
    }
    return tabTaches;
  }
  deleteTaches(){
    this.Taches.clear();
  }

}

function logMapElements(value, key, map) {
  console.log("m["+key+"] = "+JSON.stringify(value));
}


module.exports = Taches;
