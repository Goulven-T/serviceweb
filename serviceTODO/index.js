var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var RiveScript = require('rivescript');
//var dao = require('dummyTodosDAO'); // in the node_modules directory
var newbot = new RiveScript();
let Bot = require('./Bot.js');
let Bots = require('./Bots.js');
let tabBots = [];

const bots = new Bots();

// Test
const bot = new Bot({"id" : 1, "title":"Bonjour", "port" : 2001});
bots.addBot(bot);
tabBots.push(bot);
//

var app = express();

//https://expressjs.com/en/resources/middleware/cors.html

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.get('/bots',function(req, res) {
  
  res.setHeader('Content-Type', 'application/json');

  let reponse = JSON.stringify(tabBots);
  res.status(200).send(reponse);
});

app.get('/bot/:id',function(req,res){
  let pos = -1;
  var id = parseInt(req.params.id);
  console.log(id);
  tabBots.forEach(function(item,index,array){
	console.log(item.id);
	if(item.id === id){
		pos = index;
	}
  })
  if(pos != -1){
	  res.setHeader('Content-Type', 'application/json');
	  res.status(200).send(tabBots[pos]);
  }
});

app.post('/bots',function(req,res){
  let title = req.body.title;
  let brain = req.body.brain;
  let newbot = {};
  if(title != undefined && brain != undefined){
	 newbot = {"id" :  parseInt(Math.floor(Math.random() * Math.floor(100000))),"title" : title, "brain" : brain, "port" : parseInt(Math.floor(Math.random() * Math.floor(1000)))};
  }else if(title === undefined && brain != undefined){
	 newbot = {"id" :  parseInt(Math.floor(Math.random() * Math.floor(100000))),"title" : "undefined","brain" : brain,  "port" : parseInt(Math.floor(Math.random() * Math.floor(1000)))};
  }else if(title != undefined && brain == undefined){
	 newbot = {"id" :  parseInt(Math.floor(Math.random() * Math.floor(100000))),"title" : title,"brain" : "Standard.rive",  "port" : parseInt(Math.floor(Math.random() * Math.floor(1000)))};
  }else{
	 newbot = {"id" :  parseInt(Math.floor(Math.random() * Math.floor(100000))),"title" :"undefined", "brain" : "Standard.rive",  "port" : parseInt(Math.floor(Math.random() * Math.floor(1000)))};
  }console.log(newbot+" -->bot");
  var thisbot = new Bot(newbot);
  tabBots.push(thisbot);/*,function(key, value) {
    if (typeof value === 'object' && value !== null) {
        if (cache.indexOf(value) !== -1) {
            // Duplicate reference found
            try {
                // If this value does not reference a parent it can be deduped
                return JSON.parse(JSON.stringify(value));
            } catch (error) {
                // discard key if value cannot be deduped
                return;
            }
        }
        // Store value in our collection
        cache.push(value);
    }
    return value;
}));
  cache = null; // Enable garbage collection));*/
  res.status(200).send("bot creer");

});

app.delete('/bot/:id',function(req,res){
	let pos = -1;
	var id = parseInt(req.params.id);
	console.log(id);
	tabBots.forEach(function(item,index,array){
		console.log(item.id);
		if(item.id === id){
			pos = index;
		}
	})
	console.log(pos);
	if(pos != -1){
		tabBots.splice(pos,1);
		res.status(200).send("bot efface");
	}
	else{
		res.status(404).send("bot non trouve");
	}
});

app.put('/bot/:id',function(req,res){
	let nom = req.body.title;
	let port = req.body.port;
  	let brain = req.body.brain;
	if(nom !== undefined){
		let pos = -1;
		var id = parseInt(req.params.id);
		console.log(id);
		tabBots.forEach(function(item,index,array){
			console.log(item.id);
			if(item.id === id){
				pos = index;
			}
		})
		console.log(pos);
		if(pos != -1){
			let currentbot = tabBots[pos];
			console.log(currentbot);
			currentbot.setNom(nom);
		}
		else{
			res.status(404).send("impossible de changer le nom");
		}
	}if(port !== undefined){
		let pos = -1;
		var id = parseInt(req.params.id);
		console.log(id);
		tabBots.forEach(function(item,index,array){
			console.log(item.id);
			if(item.id === id){
				pos = index;
			}
		})
		console.log(pos);
		if(pos != -1){
			let currentbot = tabBots[pos];
			currentbot.setport(port);
		}
		else{
			res.status(404).send("impossible de changer le nom");
		}
	}
	if(brain !== undefined){
		let pos = -1;
		var id = parseInt(req.params.id);
		console.log(id);
		tabBots.forEach(function(item,index,array){
			console.log(item.id);
			if(item.id === id){
				pos = index;
			}
		})
		console.log(pos);
		if(pos != -1){
			let currentbot = tabBots[pos];
			currentbot.setbrain(brain);
		}
		else{
			res.status(404).send("impossible de changer le nom");
		}
	}res.status(200).send("bot modifie");
});
/*
newbot.loadFile('./brain/standard.rive',function(){
	//console.log("read success");
	newbot.sortReplies();
	ask();
}, function(error){
	console.log("Error reading file: "+error);
})

function ask(){
	
	rl.question(talking, function(message){
		
	if (talking==='you :'){
			talking = 'bot :';
		}else{
			talking = 'you :';}

		var reply = newbot.reply('local-user', message).then(function(reply) {
		console.log(Chalk.red(reply));

  // good to go
}).catch(function(error){
  // something went wrong
});
		//console.log(Chalk.red('Bot: '+ reply));
		ask();
	});
}

*/


app.listen(8081);
